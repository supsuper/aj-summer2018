﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameEventManager : MonoBehaviour {

    public static GameEventManager _instance = null;

    public delegate void DamageDealt(PlayerController target);

    public event DamageDealt DamageEvent;

	// Use this for initialization
	void Start () {
		if(_instance != null)
        {
            Debug.Log("Only one game event manager can be present per scene.");
            Destroy(gameObject);
            return;
        }

        _instance = this;
        //Debug.Log(instance);
        
        SceneManager.activeSceneChanged += SceneChanged;
    }

    public void FireDamageEvent(PlayerController target)
    {
        if(DamageEvent != null)
        {
            DamageEvent(target);
            
        }
        //Debug.Log("Damageevent fired");
    }

    public void SceneChanged(Scene current, Scene next)
    {
        SceneManager.activeSceneChanged -= SceneChanged;
        //purge instance and event listeners
        _instance = null;
        PurgeEvents();
    }

    public void PurgeEvents()
    {
        DamageEvent = null;
    }
}
