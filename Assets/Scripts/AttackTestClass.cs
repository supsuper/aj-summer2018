﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackTestClass : MonoBehaviour, IAttackController {

    public Attack attack1;
    public Attack attack2;
    public Attack attack3;
    public SpecialMove specialmove1;
    public bool attack_active = false;

    public void AttackCompleted()
    {
        attack_active = false;
    }


    // Use this for initialization
    void Start () {
        attack1.owner = this;
        attack2.owner = this;
        attack3.owner = this;
        specialmove1.owner = this;
	}
	
	// Update is called once per frame
	void Update () {
		if(attack_active == false)
        {
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                attack1.StartAttack();
                attack_active = true;
            }
            else if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                attack2.StartAttack();
                attack_active = true;
            }
            else if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                attack3.StartAttack();
                attack_active = true;
            }
            else if (Input.GetKeyDown(KeyCode.Alpha4))
            {
                specialmove1.StartAttack();
                attack_active = true;
            }
        }
	}
}
