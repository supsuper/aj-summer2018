﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowTrashbag : Move {

    public TrashBagPhysics trashbag;
    public float FireDirection = -1;
    public Transform FireOrigin;
    
    public override void OnMoveStart()
    {
        TrashBagPhysics projectile = Instantiate(trashbag, FireOrigin.transform.position, Quaternion.identity);
        //projectile.velocity = velocity;
        Invoke("OnMoveEnd", 0.75f);
    }

    public override void OnMoveEnd()
    {
        Debug.Log("Move 1 has ended!");
        base.OnMoveEnd();
    }
}
