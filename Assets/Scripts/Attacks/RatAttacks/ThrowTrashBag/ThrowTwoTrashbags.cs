﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowTwoTrashbags : Move {

    public GameObject trashbag1;
    public GameObject trashbag2;
    public float FireDirection = -1;
    public Transform FireOrigin;

    public override void OnMoveStart()
    {
        var projectile = Instantiate(trashbag1, FireOrigin.transform.position, Quaternion.identity);
        projectile = Instantiate(trashbag2, FireOrigin.transform.position, Quaternion.identity);
        Invoke("OnMoveEnd", 0.75f);
    }

    public override void OnMoveEnd()
    {
        Debug.Log("Move 1 has ended!");
        base.OnMoveEnd();
    }
}
