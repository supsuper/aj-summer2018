﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrashBagPhysics : MonoBehaviour {

    public Vector3 Gravity = new Vector3(0, -9, 0);
    float rotationSpeed = 360;

    public Vector2 velocity = new Vector2(-10, 16);
    Rigidbody2D _rigidBody;
    Collider2D _collider;

    private void Awake()
    {
        _rigidBody = GetComponent<Rigidbody2D>();
        _collider = GetComponent<Collider2D>();
        _collider.enabled = false;
    }

    private IEnumerator Start()
    {
        _rigidBody.velocity = velocity;
        yield return new WaitForSeconds(0.2f);
        _collider.enabled = true;
        
    }

    private void FixedUpdate()
    {
        _rigidBody.AddForce(Gravity);
        transform.Rotate(new Vector3(0, 0, rotationSpeed * Time.fixedDeltaTime));
    }
}
