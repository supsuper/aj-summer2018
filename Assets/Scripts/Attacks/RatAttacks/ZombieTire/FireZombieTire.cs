﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireZombieTire : Move {

    public ZombieTire zombieTire;
    public Transform FireOrigin;

    public override void OnMoveStart()
    {
        ZombieTire projectile = Instantiate(zombieTire, FireOrigin.transform.position, Quaternion.identity);
        Invoke("OnMoveEnd", 0.75f);
    }

    public override void OnMoveEnd()
    {
        base.OnMoveEnd();
    }
}
