﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieTire : MonoBehaviour {

    public Vector2 Movement = new Vector2(-10, 10);

    Collider2D _collider;

    private void Awake()
    {
        _collider = GetComponent<Collider2D>();
        _collider.enabled = false;
    }

    private IEnumerator Start()
    {
        yield return new WaitForSeconds(0.2f);
        _collider.enabled = true;
    }


    private void Update()
    {
        transform.Translate(Movement * Time.deltaTime);
    }

    public void InvertYVelocity()
    {
        Movement.y = -Movement.y;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (!collision.gameObject.CompareTag("Player"))
        {
            InvertYVelocity();
        }
    }
}
