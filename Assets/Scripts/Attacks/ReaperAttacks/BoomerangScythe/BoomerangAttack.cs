﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoomerangAttack : Move {

    public BoomerangScythe _boomerangScythe;

    public Transform FireOrigin;

    public override void OnMoveStart()
    {
        BoomerangScythe _newScythe;
        _newScythe = Instantiate(_boomerangScythe, FireOrigin.transform.position, Quaternion.identity);
        _newScythe._owner = this;
    }

    public override void OnMoveEnd()
    {
        base.OnMoveEnd();
    }
}
