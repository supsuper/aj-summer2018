﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoomerangScythe : MonoBehaviour {
    public BulletMoveTowardsPlayer targeter;
    public BoomerangAttack _owner;

    public GameObject ScytheSpriteObject;

    public GameObject BoomerangIndicator;
    public GameObject player;
    public Animator BoomerangIndicatorAnimator;
    public bool BoomerangIndicatorFollowPlayer;
    public float BoomerangIndicatorXpos = -8.5f;
    public float offScreenLimit = -0.2f;

    float MoveSpeed = -8f;
    float VerticalMoveSpeed = 4f;


    float Timer = 0;
    float indicatorTimer = 2f;
    float finalWarningTimer = 0.5f;
    float graceTimer = 2f;
    Vector2 IndicatorFixedPos;

    Collider2D _collider;

    enum BoomerangState
    {
        firing=0,
        indicatorActive,
        indicatorFixed,
        boomerangReturned
    }

    BoomerangState currentState;

    private void Awake()
    {
        targeter = GetComponent<BulletMoveTowardsPlayer>();
        _collider = GetComponent<Collider2D>();
        _collider.enabled = false;
    }

    private IEnumerator Start()
    {
        player = GameObject.Find("Player1Paddle");
        if (player == null)
        {
            Debug.Log("Was unable to find the player :(");
        }
        currentState = BoomerangState.firing;
        BoomerangIndicatorFollowPlayer = false;
        Timer = 0;
        yield return new WaitForSeconds(0.5f);
        _collider.enabled = true;
    }

    bool IsOffScreen()
    {
        Vector3 screenPoint = Camera.main.WorldToViewportPoint(transform.position);
        return (screenPoint.x < offScreenLimit || screenPoint.x > 1 - offScreenLimit);
    }

    private void Update()
    {
        switch (currentState)
        {
            case BoomerangState.firing:
                MoveBoomerangTowardPlayer();
                if (IsOffScreen())
                {
                    currentState = BoomerangState.indicatorActive;
                    BoomerangIndicatorFollowPlayer = true;
                    //BoomerangIndicatorAnimator.Play("Flash", 0);
                    BoomerangIndicator.SetActive(true);
                    BoomerangIndicatorAnimator.Play("Flash", 0);
                }
                break;
            case BoomerangState.indicatorActive:
                Timer += Time.deltaTime;
                if(Timer > indicatorTimer)
                {
                    currentState = BoomerangState.indicatorFixed;
                    BoomerangIndicatorFollowPlayer = false;
                    BoomerangIndicatorAnimator.Play("FlashFast", 0);
                    Timer = 0;
                    SetBoomerangToPlayerYPos();
                }
                break;
            case BoomerangState.indicatorFixed:
                Timer += Time.deltaTime;
                if (Timer > finalWarningTimer)
                {
                    currentState = BoomerangState.boomerangReturned;
                    BoomerangIndicator.SetActive(false);
                    Timer = 0;
                }
                break;
            case BoomerangState.boomerangReturned:
                ReturningBoomerangMovement();
                Timer += Time.deltaTime;
                if (IsOffScreen() && (Timer > graceTimer))
                {
                    EndBoomerang();
                }
                break;
        }
        ScytheSpriteObject.transform.Rotate(new Vector3(0, 0, 30));
    }

    void EndBoomerang()
    {
        //Call OnEnd if this ends up being a move
        
        Destroy(gameObject);
    }

    private void OnDestroy()
    {
        if (_owner != null)
        {
            _owner.OnMoveEnd();
        }
        else
        {
            Debug.LogWarning("Scythe's owner was null...");
        }
    }

    void MoveBoomerangTowardPlayer()
    {
        float ytarget = targeter.GetVectorToPlayer().y;
        transform.Translate(new Vector2(MoveSpeed * Time.deltaTime, ytarget * VerticalMoveSpeed * Time.deltaTime));
    }

    void SetBoomerangToPlayerYPos()
    {
        transform.position = new Vector2(transform.position.x, player.transform.position.y);
    }

    void ReturningBoomerangMovement()
    {
        transform.Translate(new Vector2(-MoveSpeed * 2 * Time.deltaTime, 0));
    }

    private void LateUpdate()
    {
        if (BoomerangIndicatorFollowPlayer)
        {
            BoomerangIndicator.transform.position = new Vector2(BoomerangIndicatorXpos,
                player.transform.position.y);
            IndicatorFixedPos = BoomerangIndicator.transform.position;
        }
        else
        {
            BoomerangIndicator.transform.position = IndicatorFixedPos;
        }
    }
}
