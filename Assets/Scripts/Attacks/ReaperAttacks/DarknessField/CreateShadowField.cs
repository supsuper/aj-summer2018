﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateShadowField : Move {

    [SerializeField] GameObject ShadowField;

    public override void OnMoveStart()
    {
        ShadowField.SetActive(true);
        Invoke("OnMoveEnd", 13);
    }

    public override void OnMoveEnd()
    {
        ShadowField.SetActive(false);
        base.OnMoveEnd();
    }
}
