﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShadowFollowPlayer : MonoBehaviour {

    [SerializeField] GameObject objectToFollow;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void LateUpdate () {
        transform.position = objectToFollow.transform.position;
	}
}
