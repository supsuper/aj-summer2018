﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoubleSkull : Move {

    public SineWaveSkull sineWaveSkullA;
    public SineWaveSkull sineWaveSkullB;
    public Transform FireOrigin;

    public override void OnMoveStart()
    {
        SineWaveSkull projectile = Instantiate(sineWaveSkullA, FireOrigin.transform.position, Quaternion.identity);
        projectile.cycleValue = Mathf.PI;
        projectile.YRange = 4;
        projectile = Instantiate(sineWaveSkullB, FireOrigin.transform.position, Quaternion.identity);
        projectile.YRange = 4;
        Invoke("OnMoveEnd", 1.5f);
    }

    public override void OnMoveEnd()
    {
        Debug.Log("Move 1 has ended!");
        base.OnMoveEnd();
    }
}
