﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootSkull : Move {

    public SineWaveSkull sineWaveSkull;
    public Transform FireOrigin;

    public override void OnMoveStart()
    {
        var projectile = Instantiate(sineWaveSkull, FireOrigin.transform.position, Quaternion.identity);
        Invoke("OnMoveEnd", 1.5f);
    }

    public override void OnMoveEnd()
    {
        Debug.Log("Move 1 has ended!");
        base.OnMoveEnd();
    }
}
