﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SineWaveSkull : MonoBehaviour {

    public float cycleValue = 0;
    public float cycleSpeed = 4;
    public float YRange = 2;
    float speed = -6;
    protected Rigidbody2D _rigidbody;
    private Collider2D _collider;
    float spawnY = 0;
    float prevYdiff = 0;
    public ParticleSystem emitter;

    // Use this for initialization
    void Awake () {
        _rigidbody = GetComponent<Rigidbody2D>();
        _collider = GetComponent<Collider2D>();
        _collider.enabled = false;
        cycleValue = 0;

    }

    private IEnumerator Start()
    {
        yield return new WaitForSeconds(0.5f);
        _collider.enabled = true;
        spawnY = transform.position.y;
        emitter.transform.parent = null;
    }

    // Update is called once per frame
    void Update () {
        cycleValue += Time.deltaTime * cycleSpeed;
        float nexty = Mathf.Sin(cycleValue) * YRange;
        float diff = nexty - prevYdiff;
        prevYdiff = nexty;
        transform.Translate(new Vector2(speed * Time.deltaTime, diff));
        //float cury = transform.position.y;
        //float diff = nexty - cury;

    }

    private void OnDestroy()
    {
        if(emitter != null)
        {
            emitter.Stop();
        }
        
        //transform.DetachChildren();
        // this stops the particle from creating more bits
        //emitter.emissionRate = 0;
        Destroy(emitter, 1);
    }

    private void LateUpdate()
    {
        emitter.transform.position = transform.position;
    }
}
