﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireLaserDrone : Move {

    public LaserDrone laserDrone;
    public Vector2 StartPosition;

    public Transform FireOrigin;
    public float timeForMoveEnd = 0.3f;

    public override void OnMoveStart()
    {
        LaserDrone lball;

        lball = Instantiate(laserDrone, FireOrigin.transform.position, Quaternion.identity);
        lball.SetStartPosition(StartPosition);

        Invoke("OnMoveEnd", timeForMoveEnd);
    }

    public override void OnMoveEnd()
    {
        base.OnMoveEnd();
    }
}
