﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireTwinDrones : Move
{

    public LaserDrone laserDrone;
    public Vector2 StartPosition1;
    public Vector2 StartPosition2;

    public Transform FireOrigin;
    public float timeForMoveEnd = 0.3f;

    public override void OnMoveStart()
    {
        LaserDrone lball;

        lball = Instantiate(laserDrone, FireOrigin.transform.position, Quaternion.identity);
        lball.SetStartPosition(StartPosition1);
        lball = Instantiate(laserDrone, FireOrigin.transform.position, Quaternion.identity);
        lball.SetStartPosition(StartPosition2);

        Invoke("OnMoveEnd", timeForMoveEnd);
    }

    public override void OnMoveEnd()
    {
        Debug.Log("Twin drones ended");
        base.OnMoveEnd();
    }
}
