﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserDrone : MonoBehaviour {

    public Vector2 StartPosition = Vector2.zero;
    float lerpToStartPositionTime = 0.5f;
    float waitTime = 0.5f;
    float laserFireTime = 1f;
    float Timer = 0;
    public Animator laserFireAnimator;

    enum LaserDroneState
    {
        moveToStartPosition = 0,
        waitForTimer,
        fireLaser,
        exitStage
    }

    LaserDroneState lbState;

    private void Awake()
    {
    }

    // Use this for initialization
    private IEnumerator Start()
    {
        lbState = LaserDroneState.moveToStartPosition;
        Timer = 0;
        yield return new WaitForSeconds(0.5f);
    }

    public void SetStartPosition(Vector2 pos)
    {
        StartPosition = pos;
    }

    // Update is called once per frame
    void Update()
    {
        Timer += Time.deltaTime;

        switch (lbState)
        {
            case LaserDroneState.moveToStartPosition:
                Vector2 lerpos = new Vector2();
                lerpos.x = Mathf.Lerp(transform.position.x, StartPosition.x, 0.5f);
                lerpos.y = Mathf.Lerp(transform.position.y, StartPosition.y, 0.5f);
                transform.position = lerpos;
                if (Timer > lerpToStartPositionTime)
                {
                    lbState = LaserDroneState.waitForTimer;
                }
                break;
            case LaserDroneState.waitForTimer:
                if (Timer >= lerpToStartPositionTime + waitTime)
                {
                    lbState = LaserDroneState.fireLaser;
                }
                break;
            case LaserDroneState.fireLaser:
                //FireLaser
                laserFireAnimator.Play("FireLaser", 0);
                
                if (Timer >= lerpToStartPositionTime + waitTime + laserFireTime)
                {
                    lbState = LaserDroneState.exitStage;
                    Invoke("DestroyDrone", 1);
                }
                break;
            case LaserDroneState.exitStage:
                transform.Translate(new Vector2(0, 6 * Time.deltaTime));
                break;
        }
    }

    void DestroyDrone()
    {
        Destroy(gameObject);
    }
}
