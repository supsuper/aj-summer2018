﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMoveTowardsPlayer : MonoBehaviour {
    GameObject target = null;
    public float angle = 0;
    public Vector2 targetVector;
	// Use this for initialization
	void Start () {
        target = GameObject.Find("Player1Paddle");
        if(target == null)
        {
            Debug.Log("Was unable to find the player :(");
        }
	}
	
	// Update is called once per frame
	void Update () {
        
    }

    public Vector3 GetVectorToPlayer()
    {
        targetVector = (target.transform.position - transform.position).normalized;
        return targetVector;
    }
}
