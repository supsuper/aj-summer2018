﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireLightningBalls : Move {

    public LightningBall lightningBall;
    public Vector2[] StartPositions = new Vector2[4];

    public Transform FireOrigin;

    public override void OnMoveStart()
    {
        LightningBall lball;
        for(int i = 0; i < 4; i++)
        {
            lball = Instantiate(lightningBall, FireOrigin.transform.position, Quaternion.identity);
            lball.SetStartPosition(StartPositions[i]);
        }
            

        Invoke("OnMoveEnd", 0.75f);
    }

    public override void OnMoveEnd()
    {
        Debug.Log("Move 1 has ended!");
        base.OnMoveEnd();
    }
}
