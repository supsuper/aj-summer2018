﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightningBall : MonoBehaviour {

    public Vector2 StartPosition = Vector2.zero;
    public float lerpToStartPositionTime = 0.5f;
    public float waitTime = 2f;
    float Timer = 0;
    public BulletMoveTowardsPlayer targeter;
    Vector2 TargetVector = Vector2.zero;
    float moveSpeed = 15;
    Rigidbody2D _rigidBody;
    Collider2D _collider;
    public Animator _lightningBallAnimator;
    public ParticleSystem _particles;

    enum LightningBallState
    {
        moveToStartPosition=0,
        waitForTimer,
        moveOut
    }

    LightningBallState lbState;

    private void Awake()
    {
        targeter = GetComponent<BulletMoveTowardsPlayer>();
        _rigidBody = GetComponent<Rigidbody2D>();
        _collider = GetComponent<Collider2D>();
        _collider.enabled = false;
    }

    // Use this for initialization
    private IEnumerator Start()
    {
        lbState = LightningBallState.moveToStartPosition;
        Timer = 0;
        yield return new WaitForSeconds(0.5f);
        _collider.enabled = true;
        _lightningBallAnimator.Play("LightningBallAnimator");
    }

    public void SetStartPosition(Vector2 pos)
    {
        StartPosition = pos;
    }
	
	// Update is called once per frame
	void Update () {
        Timer += Time.deltaTime;

        switch (lbState)
        {
            case LightningBallState.moveToStartPosition:
                Vector2 lerpos = new Vector2();
                lerpos.x = Mathf.Lerp(transform.position.x, StartPosition.x, 0.5f);
                lerpos.y = Mathf.Lerp(transform.position.y, StartPosition.y, 0.5f);
                transform.position = lerpos;
                if (Timer > lerpToStartPositionTime)
                {
                    lbState = LightningBallState.waitForTimer;
                }
                break;
            case LightningBallState.waitForTimer:
                if (Timer >= lerpToStartPositionTime + waitTime)
                {
                    lbState = LightningBallState.moveOut;
                    TargetVector = targeter.GetVectorToPlayer();
                    _lightningBallAnimator.Play("LighttningBallFiring");
                    _particles.Play();
                }
                break;
            case LightningBallState.moveOut:
                _rigidBody.velocity = new Vector2(-moveSpeed, TargetVector.y * moveSpeed);
                break;
        }
	}
}
