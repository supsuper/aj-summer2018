﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnImpactOnHittingAnything : MonoBehaviour {

    public GameObject impacteffect;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            GameObject particles = Instantiate(impacteffect, transform.position, Quaternion.identity);
        }
    }
}
