﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationMonitor : MonoBehaviour {

    bool currentAnimationComplete = false;

    public bool CurrentAnimationComplete
    {
        get
        {
            return currentAnimationComplete;
        }
    }

    public void AnimationCompleted()
    {
        
        currentAnimationComplete = true;
    }

    public void Reset()
    {
        currentAnimationComplete = false;
        Debug.Log("Animation monitor Reset");
    }
}
