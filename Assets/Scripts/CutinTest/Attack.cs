﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : MonoBehaviour {

    [SerializeField] List<Move> Moves;
    public IAttackController owner;
    //bool attack_completed = false;
    bool attack_activated = false;
    int current_move_index = 0;
    
    //on activation, go through each move until they're finished.

    private void Awake()
    {
        foreach(Move move in Moves)
        {
            move._attack = this;
        }
    }

    public void StartAttack()
    {
        attack_activated = true;
        Moves[current_move_index].OnMoveStart();
        Debug.Log("Number of moves: " + Moves.Count);
    }

    private void Update()
    {
        if (attack_activated)
        {
            Moves[current_move_index].UpdateMove();
            Debug.Log("Number of moves: " + Moves.Count);
        }
    }

    public void OnMoveFinished()
    {
        current_move_index++;
        Debug.Log("Move index:" + current_move_index + " of " + Moves.Count);
        if (current_move_index >= Moves.Count)
        {
            //Attack completed!
            //attack_completed = true;
            if(owner != null)
            {
                owner.AttackCompleted();
                ResetAttack();
            }
            //call owning special?
        }
        else
        {
            //Start the next move
            Moves[current_move_index].OnMoveStart();
        }

        
    }

    public void ResetAttack()
    {
        //attack_completed = false;
        Debug.Log("Reset Attack");
        attack_activated = false;
        current_move_index = 0;
    }

}