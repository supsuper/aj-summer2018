﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutinTester : MonoBehaviour {

    AnimationMonitor _animationMonitor;
    Animator _spriteAnimator;
    float CutinTimeScale = 0.1f;
    SpecialMove _specialMove;

    private void Awake()
    {
        _animationMonitor = GetComponentInChildren<AnimationMonitor>();
        _spriteAnimator = GetComponentInChildren<Animator>();
    }

    private void Start()
    {
        
    }

    private void Update()
    {
        if (_animationMonitor.CurrentAnimationComplete)
        {
            Debug.Log("Animation completed");
            _animationMonitor.Reset();
            PongTimeScaler.ResetTimeScale();
        }
        if (Input.GetButtonDown("Fire1"))
        {
            PlayAnimation();
        }
    }

    void PlayAnimation()
    {
        _spriteAnimator.Play("CutinTestAnimation");
        PongTimeScaler.TimeScale = CutinTimeScale;
    }
}
