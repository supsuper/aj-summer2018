﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FakeBallDriver : MonoBehaviour {

    float currentDirection = 1;
    float speed = 10f;
    Animator _animator;
    string RemoveAnimation = "FakeBallRemove";
    string DefaultAnimation = "FakeBallDraw";
    Vector2 movement = Vector2.zero;
    bool ball_removed = false;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        currentDirection *= -1;
        movement.y = 0; //This is just for demonstration, to keep the ball in the middle of the screen.
    }

    // Use this for initialization
    void Start () {
        _animator = GetComponent<Animator>();
        _animator.Play(DefaultAnimation);
	}
	
	// Update is called once per frame
	void Update () {
        if (!ball_removed)
        {
            Move();
        }  
	}

    private void Move()
    {
        movement.x = speed * Time.deltaTime * currentDirection * PongTimeScaler.TimeScale;
        transform.Translate(movement);
    }

    public void Remove()
    {
        _animator.Play(RemoveAnimation);
        ball_removed = true;
    }

    public void ReturnToPlay()
    {
        transform.position = new Vector2(0, 7); //Move to top-center off-screen
        currentDirection = -1; //Move towards the player
        _animator.Play(DefaultAnimation);
        movement.y = -speed * Time.deltaTime;
        ball_removed = false;
    }
}
