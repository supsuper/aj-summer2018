﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FakeGameManager : MonoBehaviour {

    public static FakeGameManager instance = null;

    [SerializeField] FakeBallDriver ball;
    [SerializeField] Animator ballReturnIndicator;
    string BallReturningAnimationName = "BallReturning";

    // Use this for initialization
    void Start () {
		if(instance != null)
        {
            Debug.LogWarning("There should only be one FakeGameManager");
            Destroy(gameObject);
            return;
        }
        instance = this;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void RemoveBall()
    {
        ball.Remove();
    }

    public void ShowReturnBallIndicator()
    {
        ballReturnIndicator.Play(BallReturningAnimationName);
    }

    public void ReturnBall()
    {
        ball.ReturnToPlay();
    }
}
