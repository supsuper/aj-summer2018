﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Move : MonoBehaviour {

    public Attack _attack;

    public virtual void OnMoveStart()
    {
        //initialise things needed for the attack
    }

	public virtual void UpdateMove()
    {
        //update the components of the attack if needed
        //poll completion conditions
    }

    public virtual void OnMoveEnd()
    {
        _attack.OnMoveFinished();
        ResetMove();
    }

    public virtual void ResetMove()
    {

    }
}
