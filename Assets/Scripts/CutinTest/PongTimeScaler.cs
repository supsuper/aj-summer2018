﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PongTimeScaler : MonoBehaviour {

    static float _timeScale = 1;
    static float DefaultTimeScale = 1;

    public static float TimeScale
    {
        get
        {
            return _timeScale;
        }

        set
        {
            _timeScale = value;
        }
    }

    private void Awake()
    {
        TimeScale = DefaultTimeScale;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public static void ResetTimeScale()
    {
        TimeScale = DefaultTimeScale;
    }
}
