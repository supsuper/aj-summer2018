﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using StateMachine;
using System;

public class SpecialMove : MonoBehaviour, IAttackController {

    AnimationMonitor _animationMonitor;
    Animator _animator;
    string _animationStateName = "CutinTestAnimation";
    [SerializeField] Attack attack;
    public IAttackController owner;

    public enum SpecialMoveState
    {
        start=0,
        prepare_for_cutin,
        cutin,
        during_attack,
        post_attack
    }

    protected bool removeBallFromPlay = false;
    protected bool removeFriendlyPaddleFromPlay = false;
    protected bool removeEnemyPaddleFromPlay = false;
    protected bool slowtime = true;
    protected bool hasCutin = true;
    protected bool special_active = false;
    protected float slowedTimeScale = 0f;
    SpecialMoveState _currentState = SpecialMoveState.start;

    public bool RemoveBallFromPlay
    {
        get
        {
            return removeBallFromPlay;
        }
    }
    public bool RemoveFriendlyPaddleFromPlay
    {
        get
        {
            return removeFriendlyPaddleFromPlay;
        }
    }
    public bool RemoveEnemyPaddleFromPlay
    {
        get
        {
            return removeEnemyPaddleFromPlay;
        }
    }
    public bool Slowtime
    {
        get
        {
            return slowtime;
        }
    }
    public bool HasCutin
    {
        get
        {
            return hasCutin;
        }
    }
    public float SlowedTimeScale
    {
        get
        {
            return slowedTimeScale;
        }
    }
    public SpecialMoveState CurrentState
    {
        get
        {
            return _currentState;
        }
    }


    FiniteStateMachine _fsm;
    public State_SpecialStart state_start;
    public State_PlayCutin state_cutin;
    public State_DuringAttack state_during;
    public State_PostAttack state_postattack;
    public State_Idle state_idle;

    // Use this for initialization
    protected virtual void Awake () {

        _animationMonitor = GetComponentInChildren<AnimationMonitor>();
        _animator = GetComponentInChildren<Animator>();


        _fsm = new FiniteStateMachine();

        state_start = new State_SpecialStart(this, _fsm);
        state_cutin = new State_PlayCutin(this, _fsm);
        state_during = new State_DuringAttack(this, _fsm, attack);
        state_postattack = new State_PostAttack(this, _fsm);
        state_idle = new State_Idle(this, _fsm);

        _fsm.ChangeState(state_idle);
	}
	
	// Update is called once per frame
	void Update () {
        //if (Input.GetButtonDown("Fire1") && !special_active)
        //{
        //    StartAttack();
        //}
        _fsm.Update();
	}

    public void StartAttack()
    {
        if (special_active)
        {
            return;
        }
        _fsm.ChangeState(state_start);
        special_active = true;
    }

    public void PlayAnimation()
    {
        _animator.Play(_animationStateName);
    }

    public bool CutinAnimationComplete()
    {
        return _animationMonitor.CurrentAnimationComplete;
    }

    public bool AttackComplete()
    {
        return true;
    }

    public virtual void RemoveSpecialAttackObjects()
    {

    }

    public virtual void ResetSpecial()
    {
        special_active = false;
        owner.AttackCompleted();
    }

    public void SetCutinTimeScale()
    {
        //PongTimeScaler.TimeScale = SlowedTimeScale;
        Time.timeScale = SlowedTimeScale;
    }

    public void ResetTimeScale()
    {
        //PongTimeScaler.ResetTimeScale();
        Time.timeScale = 1;
    }

    public void ResetAnimationMonitor()
    {
        _animationMonitor.Reset();
    }

    public void AttackCompleted()
    {
        throw new NotImplementedException();
    }
}

public class State_SpecialStart : FSM_State
{
    SpecialMove _special;

    float State_Timer = 0;
    float State_Duration = 0.3f;

    public State_SpecialStart(SpecialMove special, FiniteStateMachine fsm) : base(fsm)
    {
        _special = special;
    }

    public override void FixedUpdate()
    {
        
    }

    public override void OnEnter()
    {
        Debug.Log("Special attack started!");
        State_Timer = 0;
        if (_special.RemoveBallFromPlay || _special.RemoveEnemyPaddleFromPlay || _special.RemoveFriendlyPaddleFromPlay)
        {
            //start timer
            if (_special.RemoveBallFromPlay)
            {
                FakeGameManager.instance.RemoveBall();
            }

            if (_special.RemoveEnemyPaddleFromPlay)
            {
                //GameManager.RemoveEnemyPaddleFromPlay();
            }

            if (_special.RemoveFriendlyPaddleFromPlay)
            {
                //GameManager.RemoveFriendlyPaddleFromPlay();
            }
        }
        else
        {
            ChangeToNextState();
            return;
        }
    }

    void ChangeToNextState()
    {
        if (_special.HasCutin)
        {
            _fsm.ChangeState(_special.state_cutin);
        }
        else
        {
            _fsm.ChangeState(_special.state_during);
        }
    }

    public override void OnExit()
    {
        Debug.Log("Start state ended!");
    }

    public override void Update()
    {
        State_Timer += Time.deltaTime;
        if(State_Timer > State_Duration)
        {
            ChangeToNextState();
        }
    }

   
}

public class State_PlayCutin : FSM_State
{
    SpecialMove _special;

    public State_PlayCutin(SpecialMove special, FiniteStateMachine fsm) : base(fsm)
    {
        _special = special;
    }

    public override void FixedUpdate()
    {
        
    }

    public override void OnEnter()
    {
        Debug.Log("Cutin state entered");
        _special.SetCutinTimeScale();
        _special.PlayAnimation();
    }

    public override void OnExit()
    {
        Debug.Log("Cutin animation complete");
        _special.ResetTimeScale();
        PongTimeScaler.ResetTimeScale();
    }

    public override void Update()
    {
        if (_special.CutinAnimationComplete())
        {
            _fsm.ChangeState(_special.state_during);
            _special.ResetAnimationMonitor();
        }
    }
}

public class State_DuringAttack : FSM_State, IAttackController
{
    SpecialMove _special;
    Attack _attack;

    public State_DuringAttack(SpecialMove special, FiniteStateMachine fsm, Attack attack) : base(fsm)
    {
        _special = special;
        _attack = attack;
        _attack.owner = this;
    }

    public void AttackCompleted()
    {
        _fsm.ChangeState(_special.state_postattack);
        Debug.Log("Attack completed");
    }

    public override void FixedUpdate()
    {

    }

    public override void OnEnter()
    {
        Debug.Log("Attack state started");
        _attack.StartAttack();
    }

    public override void OnExit()
    {
        Debug.Log("Attack state ended");
    }

    public override void Update()
    {
        //when the attack is finished
        //not sure yet how to determine this
        
    }
}

public class State_PostAttack : FSM_State
{
    SpecialMove _special;

    float State_Timer = 0;
    float State_Duration = 1.0f;

    public State_PostAttack(SpecialMove special, FiniteStateMachine fsm) : base(fsm)
    {
        _special = special;
    }

    public override void FixedUpdate()
    {

    }

    public override void OnEnter()
    {
        State_Timer = 0;
        Debug.Log("Post attack started");
        //Remove anything from the field that the special added
        _special.RemoveSpecialAttackObjects();
        
        if (_special.RemoveBallFromPlay || _special.RemoveEnemyPaddleFromPlay || _special.RemoveFriendlyPaddleFromPlay)
        {
            //start timer
            if (_special.RemoveBallFromPlay)
            {
                FakeGameManager.instance.ShowReturnBallIndicator();
            }

            if (_special.RemoveEnemyPaddleFromPlay)
            {
                //GameManager.ReturnEnemyPaddleToPlay();
            }

            if (_special.RemoveFriendlyPaddleFromPlay)
            {
                //GameManager.ReturnFriendlyPaddleToPlay();
            }
        }
        else
        {
            //change to next state
            return;
        }
    }

    public override void OnExit()
    {
        Debug.Log("Post attack ended.");
        if (_special.RemoveBallFromPlay)
        {
            FakeGameManager.instance.ReturnBall();
        }
        _special.ResetSpecial();
    }

    public override void Update()
    {
        State_Timer += Time.deltaTime;
        if (State_Timer > State_Duration)
        {
            _fsm.ChangeState(_special.state_idle);
        }
    }
}

public class State_Idle : FSM_State
{
    SpecialMove _special;

    public State_Idle(SpecialMove special, FiniteStateMachine fsm) : base(fsm)
    {
        _special = special;
    }

    public override void FixedUpdate()
    {

    }

    public override void OnEnter()
    {
        
    }

    public override void OnExit()
    {

    }

    public override void Update()
    {
        
    }


}