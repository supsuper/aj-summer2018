﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestMove1 : Move {

    public override void OnMoveStart()
    {
        Debug.Log("Move 1 has begun!!");
        Invoke("OnMoveEnd", 4);
    }

    public override void OnMoveEnd()
    {
        Debug.Log("Move 1 has ended!");
        base.OnMoveEnd();
    }
}
