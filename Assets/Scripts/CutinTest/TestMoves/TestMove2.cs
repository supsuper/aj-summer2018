﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestMove2 : Move {

    int bulletFiredCount = 0;
    int bulletsToFire = 3;
    float timeBetweenBullets = 1f;
    float betweenBulletTimer = 0f;
    bool bulletFired = false;

    public override void OnMoveStart()
    {
        base.OnMoveStart();
        Debug.Log("Fire 3 bullets!");
    }

    public override void UpdateMove()
    {    
        if (!bulletFired)
        {
            Debug.Log("Pew pew!");
            bulletFired = true;
            bulletFiredCount++;
        }
        else
        {
            betweenBulletTimer += Time.deltaTime;
            if(betweenBulletTimer > timeBetweenBullets)
            {
                bulletFired = false;
                betweenBulletTimer = 0;
                if (bulletFiredCount >= bulletsToFire)
                {
                    OnMoveEnd();
                }
            } 
        } 
    }

    public override void ResetMove()
    {
        base.ResetMove();
        bulletFiredCount = 0;
        betweenBulletTimer = 0f;
        bulletFired = false;
    }

    public override void OnMoveEnd()
    {
        Debug.Log("Move 2 has ended");
        base.OnMoveEnd();
    }
}
