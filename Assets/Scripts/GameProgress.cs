﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameProgress
{
	public static bool Tutorial = false;

	public static int Level
	{
		get
		{
			return PlayerPrefs.GetInt("Level", 1);
		}
		set
		{
			PlayerPrefs.SetInt("Level", value);
		}
	}

	public static string GetLevelScene()
	{
		switch (Level)
		{
			case 1: return "RatAttackTest";
			case 2: return "RobotAttackTest";
			case 3: return "ReaperAttackTest";
			default: return "Menu";
		}
	}

	public static string GetCutScene()
	{
		switch (Level)
		{
			case 1: return "CutsceneRat";
			case 2: return "CutsceneRobot";
			case 3: return "CutsceneReaper";
			default: return "Menu";
		}
	}
}