﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoomGuyFaceController : MonoBehaviour {

    [SerializeField] PlayerController owner;
    [SerializeField] Animator animator;
	float myNormalizedHP = 1;
    float enemyNormalizedHP = 1;
    float dangerHPThreshold = 0.4f;

    enum AnimState
    {
        idle=0,
        winning, 
        losing,
        victory,
        defeat,
        hitenemy,
        tookhit
    }

	// Use this for initialization
	void Start () {
        GameEventManager._instance.DamageEvent += OnDamageEvent;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnDamageEvent(PlayerController pc)
    {
        if(pc == null)
        {
            Debug.LogWarning("PlayerController passed into event was null");
            return;
        }

        //if it isn't the owner
        if (!pc.gameObject.CompareTag(owner.gameObject.tag))
        {
            animator.Play("HitEnemy", 0);
            enemyNormalizedHP = pc.Health / pc.MaxHealth;
        }
        else if (pc.gameObject.CompareTag(owner.gameObject.tag))
        {
            animator.Play("TookHit", 0);
            myNormalizedHP = owner.Health / owner.MaxHealth;
        }
        UpdateAnimationState();
    }

    void UpdateAnimationState()
    {
        if(enemyNormalizedHP <= 0)
        {
            //Victory!
            animator.Play("Victory", 0, 0);
			return;
        }else if(myNormalizedHP <= 0)
        {
            //Defeat
            animator.Play("Defeat", 0, 0);
			return;
        }

		if ((enemyNormalizedHP >= myNormalizedHP) && (myNormalizedHP < dangerHPThreshold))
        {
            animator.SetInteger("CharState", (int)AnimState.losing);
        }else if ((enemyNormalizedHP <= myNormalizedHP) && (enemyNormalizedHP < dangerHPThreshold))
        {
            animator.SetInteger("CharState", (int)AnimState.winning);
        }
        else
        {
            //Debug.Log("Set to idle");
            animator.SetInteger("CharState", (int)AnimState.idle);
        }
    }
}
