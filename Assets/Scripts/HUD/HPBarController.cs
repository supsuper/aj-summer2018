﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HPBarController : MonoBehaviour
{

	[SerializeField] Image PlayerHPBar;
	[SerializeField] Animator PlayerHpBarShaker;
	[SerializeField] Animator PlayerHPBarFlasher;
	float PlayerHpBarLastFrameFillAmount = 0;
	[SerializeField] PlayerController PlayerController;
	[SerializeField] string PlayerHpBarState;

	// Update is called once per frame
	void Update()
	{
		UpdatePlayerHpBar();
	}

	void UpdatePlayerHpBar()
	{
		PlayerHpBarLastFrameFillAmount = PlayerHPBar.fillAmount;
		UpdateHPBar(PlayerController, PlayerHPBar);
		PlayerHPBarFlasher.SetFloat("NormalizedHealth", PlayerHPBar.fillAmount);
		if (PlayerHpBarLastFrameFillAmount != PlayerHPBar.fillAmount)
		{
			PlayerHpBarShaker.Play(PlayerHpBarState, 0);
		}
	}

	void UpdateHPBar(PlayerController pc, Image img)
	{
		img.fillAmount = pc.Health / pc.MaxHealth;
		img.color = GetFillColor(img.fillAmount);
	}

	Color GetFillColor(float fillAmount)
	{
		return new Color(1 - fillAmount, fillAmount, 0);
	}
}
