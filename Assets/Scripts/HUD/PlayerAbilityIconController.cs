﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerAbilityIconController : MonoBehaviour {

    [SerializeField] PlayerAbility _ability;
    [SerializeField] Image CoolDownCover;
    [SerializeField] Text CooldownText;
    [SerializeField] Animator CooldownFlasher;
    public bool on_cooldown = false;

    public bool OnCooldown{
        set
        {
            if (!value && on_cooldown)
            {
                //Flash the icon
                CooldownFlasher.Play("AbilityCooldownFlasher", 0);
            }else if (value && !on_cooldown)
            {
                CooldownFlasher.Play("AbilityUsedFlasher", 0);
            }
            on_cooldown = value;
        }
        get
        {
            return on_cooldown;
        }
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(_ability.Cooldown > 0)
        {
            OnCooldown = true;
        }
        else
        {
            OnCooldown = false;
        }

        float normalizedCooldown = _ability.Cooldown / _ability.CooldownMax;
        CoolDownCover.fillAmount = normalizedCooldown;

        CooldownText.enabled = OnCooldown;
        if (CooldownText.enabled)
        {
            string cd = Mathf.Ceil(_ability.Cooldown).ToString();
            CooldownText.text = cd;
        }
	}
}
