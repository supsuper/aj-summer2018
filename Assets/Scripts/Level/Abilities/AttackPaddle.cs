﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackPaddle : MonoBehaviour
{
    public float DamageOnImpact = 10;
    public GameObject ImpactEffect;

    private void OnCollisionEnter2D(Collision2D collision)
	{
		if (collision.gameObject.CompareTag("Player"))
		{
			collision.gameObject.GetComponent<PaddleController>().Player.Health -= DamageOnImpact;
            SpawnImpactEffect();
			Destroy(gameObject);
		}
	}

    void SpawnImpactEffect()
    {
        if(ImpactEffect == null)
        {
            return;
        }
        Instantiate(ImpactEffect, transform.position, Quaternion.identity);
    }

	private void OnTriggerEnter2D(Collider2D collision)
	{
		Destroy(gameObject, 1f);
	}
}
