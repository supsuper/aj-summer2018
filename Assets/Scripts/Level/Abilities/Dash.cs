﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dash : MonoBehaviour, IPlayerAbility
{
	public float GottaGoFast = 2f;
	private PaddleController _paddle;
	private SpriteRenderer _sprite;
    AudioSource _audioSource;

    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    public void Setup(PlayerController player)
	{
		_paddle = player.Paddle;
		_sprite = _paddle.GetComponentInChildren<SpriteRenderer>();
	}

	// Use this for initialization
	private void Start()
	{
		_paddle.Speed *= GottaGoFast;
		_sprite.color = Color.green;
        _audioSource.Play();
	}

	private void OnDestroy()
	{
		_paddle.Speed /= GottaGoFast;
		_sprite.color = Color.white;
	}
}
