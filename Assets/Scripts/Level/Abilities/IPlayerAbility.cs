﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPlayerAbility
{
	void Setup(PlayerController player);
}
