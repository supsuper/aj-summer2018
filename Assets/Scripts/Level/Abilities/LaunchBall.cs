﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaunchBall : MonoBehaviour, IPlayerAbility
{
	public float GottaGoFast = 2f;
	private BallController _ball;
	private int _direction;

	public void Setup(PlayerController player)
	{
		_ball = player.Ball;
		_direction = player.Direction;
        //set a flag on the player paddle?
	}

	// Use this for initialization
	void Start()
	{
		_ball.Launch(_ball.Speed * GottaGoFast, _direction);
	}
}
