﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveDirection : MonoBehaviour
{
	public Vector3 Velocity;
	protected Rigidbody2D _rigidbody;
	private Collider2D _collider;

	private void Awake()
	{
		_rigidbody = GetComponent<Rigidbody2D>();
		_rigidbody.velocity = Velocity;
		_collider = GetComponent<Collider2D>();
        _collider.enabled = false;
    }

	private IEnumerator Start()
	{
		yield return new WaitForSeconds(0.5f);
		_collider.enabled = true;
	}
}
