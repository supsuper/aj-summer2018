﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shield : MonoBehaviour, IPlayerAbility
{
	private PlayerController _player;

    public void Setup(PlayerController player)
	{
		_player = player;
		_player.Shield = true;
		transform.SetParent(_player.Paddle.transform, false);
		transform.localPosition = Vector3.zero;
    }

	private void OnDestroy()
	{
		_player.Shield = false;
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.gameObject.layer == LayerMask.NameToLayer("Attack"))
		{
			Destroy(collision.gameObject);
		}
	}
}
