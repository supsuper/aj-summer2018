﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimedDestroy : MonoBehaviour
{
	public float Timer;

	// Use this for initialization
	private void Start()
	{
		Destroy(gameObject, Timer);
	}
}
