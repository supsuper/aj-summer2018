﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AiController : PlayerController, IAttackController
{
	public float AttackTime = 5;
	public float Intelligence = 0;
	public Attack[] Attacks;
	public SpecialMove SpecialAbility;

	private int _input = 0;
	private bool _attackActive = false;
	private float _nextAttack;
	private bool _special = false;

	public void AttackCompleted()
	{
		_attackActive = false;
	}

	protected override void Awake()
	{
		base.Awake();
		foreach (Attack attack in Attacks)
		{
			attack.owner = this;
		}
		SpecialAbility.owner = this;
		ResetAttackTimer();
	}

	private void Update()
	{
		FollowBall();
		Paddle.Move(_input);

		if (_attackActive == false)
		{
			_nextAttack -= Time.deltaTime;
			if (!_special && Health <= MaxHealth / 2)
			{
				SpecialAttack();
			}
			else
			{
				RandomAttack();
			}
		}

		Debug();
	}

	private void FollowBall()
	{
		_input = 0;
		if (Ball.transform.position.x > -Intelligence)
		{
			_input = (int)Mathf.Sign(Ball.transform.position.y - transform.position.y);
		}
	}

	private void ResetAttackTimer()
	{
		_nextAttack = Random.Range(AttackTime - 1, AttackTime + 1);
	}

	private void RandomAttack()
	{
		if (_nextAttack <= 0)
		{
			int attack = Random.Range(0, Attacks.Length);
			Attacks[attack].StartAttack();
			_attackActive = true;
			ResetAttackTimer();
		}
	}

	private void SpecialAttack()
	{
		SpecialAbility.StartAttack();
		_attackActive = true;
		_special = true;
		ResetAttackTimer();
	}

	private void Debug()
	{
#if UNITY_EDITOR
		if (_attackActive == false)
		{
			if (Input.GetKeyDown(KeyCode.Alpha1))
			{
				Attacks[0].StartAttack();
				_attackActive = true;
			}
			else if (Input.GetKeyDown(KeyCode.Alpha2))
			{
				Attacks[1].StartAttack();
				_attackActive = true;
			}
			else if (Input.GetKeyDown(KeyCode.Alpha3))
			{
				Attacks[2].StartAttack();
				_attackActive = true;
			}
			else if (Input.GetKeyDown(KeyCode.Alpha4))
			{
				SpecialAbility.StartAttack();
				_attackActive = true;
			}
		}
#endif
	}
}
