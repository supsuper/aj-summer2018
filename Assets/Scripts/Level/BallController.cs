﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{
	public float Speed = 5.0f;
	public float MaxAngle = 60.0f;

	private Rigidbody2D _rigidbody;
	private Collider2D _collider;
	private TrailRenderer _trail;
	private ParticleSystem _particle;
	private AudioSource _audio;

	private Vector2 _velocity;
	private ContactPoint2D[] _contacts = new ContactPoint2D[2];

    // Use this for initialization
    private void Awake()
	{
		_rigidbody = GetComponent<Rigidbody2D>();
		_collider = GetComponent<Collider2D>();
		_trail = GetComponent<TrailRenderer>();
		_particle = GetComponent<ParticleSystem>();
		_audio = GetComponent<AudioSource>();
		_collider.enabled = false;
	}

	public void Respawn()
	{
		transform.position = Vector3.zero;
		_velocity = Vector2.zero;
		_rigidbody.velocity = Vector2.zero;
		_trail.Clear();
	}

	private void FixedUpdate()
	{
        /*
        if (Mathf.Abs(_rigidbody.velocity.x) <= 1f && _collider.enabled)
		{
			StartMoving();
		}
        */
        //_rigidbody.velocity = _velocity;


        //Alternative movement
        if (Mathf.Abs(_velocity.x) <= 1f && _collider.enabled)
        {
            StartMoving();
        }
        transform.Translate(_velocity * Time.fixedDeltaTime);
    }

    private void LateUpdate()
    {
        //_rigidbody.velocity = _velocity;
    }

    public void StartMoving()
	{
		int startDirection = Random.Range(-1, 2);
		if (startDirection == 0)
			startDirection = 1;
		Launch(Speed, startDirection);
		_collider.enabled = true;
	}

	public void Launch(float speed, int direction)
	{
		float randomAngle = Random.Range(-MaxAngle, MaxAngle);
		_velocity = AngledVector(randomAngle, direction) * speed;
	}

	private Vector2 AngledVector(float angle, int direction)
	{
        Vector2 vec = Quaternion.Euler(0, 0, angle) * Vector2.right;

        vec.x *= direction;
        return vec;
        //return Quaternion.Euler(0, 0, angle) * direction;
    }

	private void OnCollisionEnter2D(Collision2D collision)
	{
		if (collision.gameObject.CompareTag("Player"))
		{
            var paddle = collision.gameObject.GetComponent<PaddleController>();
			var collider = collision.gameObject.GetComponent<Collider2D>();
			float diff = (gameObject.transform.position.y) - collision.gameObject.transform.position.y / collider.bounds.extents.y;
            float angle = Mathf.Clamp(diff, -1f, 1f) * MaxAngle;
            //float angle = Mathf.Min(diff, 1f) * MaxAngle;
            _velocity = AngledVector(angle, paddle.Player.Direction) * Speed;
            //make paddle play animation on impact
            Animator animator = collision.gameObject.GetComponent<Animator>();
            if (animator != null)
            {
                animator.Play("PaddleHitBall", 0);
            }
            //Debug.Log("paddle y: " + collision.gameObject.transform.position.y + " - ballY: " + gameObject.transform.position.y + " / extentsY: " + collider.bounds.extents.y);
        }
		else
		{
            collision.GetContacts(_contacts);
			Vector2 normal = _contacts[0].normal;
            _velocity = Vector2.Reflect(_velocity, normal);
            //_velocity.y = -_velocity.y;
		}
		_particle.Play();
		_audio.pitch = Random.Range(0.7f, 1.3f);
		_audio.PlayOneShot(_audio.clip);
	}

	private void OnCollisionStay2D(Collision2D collision)
	{
		if (collision.gameObject.CompareTag("Player"))
		{
			var collider = collision.gameObject.GetComponent<Collider2D>();
			ColliderDistance2D distance = _rigidbody.Distance(collider);
			if (distance.isValid && distance.isOverlapped)
			{
				transform.Translate(distance.normal * distance.distance);
			}
            
        }
        else
        {
            //The following prevents the ball getting stuck only if motion is done via transform.Translate, NOT by rigidbody
            _velocity.y = -_velocity.y;
            //The following appears to help prevent the ball getting stuck on a wall if using rigidbody manipulation.
            //Nevermind, it doesn't work.
            //_rigidbody.velocity = new Vector2(_rigidbody.velocity.x, -_rigidbody.velocity.y);
            Debug.Log("FlipY");
        }
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		_collider.enabled = false;
		DeathZone zone = collision.gameObject.GetComponent<DeathZone>();
		if (zone != null)
		{
			StartCoroutine(zone.Hit());
		}
	}
}
