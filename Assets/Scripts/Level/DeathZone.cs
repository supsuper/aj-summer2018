﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathZone : MonoBehaviour
{
	public PlayerController Player;
	public PongManager Game;

	public IEnumerator Hit()
	{
		Player.Health -= 10;
		yield return DelayedRound();
	}

	private IEnumerator DelayedRound()
	{
		yield return new WaitForSeconds(0.5f);
		yield return Game.NewRound();
	}
}
