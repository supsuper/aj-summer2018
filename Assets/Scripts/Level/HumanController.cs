﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HumanController : PlayerController
{
	private PlayerAbility[] _abilities;

	protected override void Awake()
	{
		base.Awake();
		_abilities = GetComponentsInChildren<PlayerAbility>();
	}

	// Update is called once per frame
	private void Update()
	{
		float y = Input.GetAxisRaw("Vertical");
		Paddle.Move(y);

		for (int i = 0; i < _abilities.Length; i++)
		{
			int key = i + 1;
			if (Input.GetButtonDown("Fire" + key))
			{
				_abilities[i].Use();
			}
		}
	}
}
