﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaddleController : MonoBehaviour
{
	public float Speed = 5f;
	public Collider2D Top;
	public Collider2D Bottom;
	public PlayerController Player;
    public Animator PaddleAnimator;

	private Rigidbody2D _rigidbody;
	private Vector3 _startPos;

	// Use this for initialization
	private void Awake()
	{
		_rigidbody = GetComponent<Rigidbody2D>();
		_startPos = transform.position;
        PaddleAnimator = GetComponent<Animator>();

    }

	public void Respawn()
	{
		transform.position = _startPos;
	}

	public void Move(float y)
	{
		Vector2 velocity = Vector2.up * y;
		velocity = velocity.normalized * Speed;

		ColliderDistance2D edgeDistance = new ColliderDistance2D();
		if (velocity.y > 0)
		{
			edgeDistance = _rigidbody.Distance(Top);
		}
		else if (velocity.y < 0)
		{
			edgeDistance = _rigidbody.Distance(Bottom);
		}
		if (edgeDistance.isValid && edgeDistance.distance < Mathf.Abs(velocity.y) * Time.fixedDeltaTime)
		{
			velocity.y = Mathf.Sign(velocity.y) * edgeDistance.distance * Time.fixedDeltaTime;
		}

		_rigidbody.velocity = velocity;
	}
}
