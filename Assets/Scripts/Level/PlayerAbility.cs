﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAbility : MonoBehaviour
{
	public GameObject Spawn;
	public float CooldownMax;
	[HideInInspector]
	public float Cooldown;

	private PlayerController _player;

	private void Start()
	{
		_player = GetComponentInParent<PlayerController>();
		Cooldown = CooldownMax;
	}

	private void Update()
	{
		Cooldown = Mathf.Max(0, Cooldown - Time.deltaTime);
	}

	public void Use()
	{
		if (Cooldown == 0)
		{
			var projectile = Instantiate(Spawn, _player.Paddle.transform.position, Quaternion.identity);

			if (projectile.GetComponent<IPlayerAbility>() != null)
				projectile.GetComponent<IPlayerAbility>().Setup(_player);

			Cooldown = CooldownMax;
		}
	}
}
