﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
	public BallController Ball;
	public PaddleController Paddle;
	public int Direction = 1;
	public float MaxHealth = 100;

    private float _health = 0;
    public float Health
    {
        get
        {
            return _health;
        }
        set
        {
			if (!Shield)
			{
				if (value < _health)
				{
					_health = value;
					FireDamageEvent();
					if (Paddle.PaddleAnimator != null)
					{
						Paddle.PaddleAnimator.Play("PaddleImpact", 0);
					}
				}
				else
				{
					_health = value;
				}
			}
        }
    }
    public Animator screenFlashAnimator;
	public bool Shield = false;

	private AudioSource _audio;
	public AudioClip[] HitSound;

	protected virtual void Awake()
	{
        _health = MaxHealth;
		_audio = GetComponent<AudioSource>();
	}

    void FireDamageEvent()
    {
		if (HitSound.Length > 0)
		{
			_audio.pitch = Random.Range(0.9f, 1.1f);
			_audio.clip = HitSound[Random.Range(0, HitSound.Length)];
			_audio.Play();
		}
        GameEventManager._instance.FireDamageEvent(this);
    }
}
