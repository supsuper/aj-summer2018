﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PongState
{
	Playing,
	Victory,
	Defeat
}

public class PongManager : MonoBehaviour
{
	public BallController Ball;
	public PlayerController Player1;
	public PlayerController Player2;
	public PauseController Pause;
	public PongState State { get; private set; }

	// Use this for initialization
	private IEnumerator Start()
	{
		State = PongState.Playing;
		GameEventManager._instance.DamageEvent += OnDamageEvent;
		yield return NewRound();
	}

	public IEnumerator NewRound()
	{
		Ball.Respawn();
		yield return new WaitForSeconds(1f);

		Ball.StartMoving();
	}

	private void Update()
	{
		if (Input.GetKeyDown(KeyCode.R))
		{
			StartCoroutine(NewRound());
		}
	}

	void OnDamageEvent(PlayerController pc)
	{
		if (Player2.Health <= 0)
		{
			State = PongState.Victory;
			Pause.Victory();
		}
		else if (Player1.Health <= 0)
		{
			State = PongState.Defeat;
			Pause.Defeat();
		}
	}
}
