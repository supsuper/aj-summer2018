﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{
	public GameObject MainMenu;
	public GameObject OptionsMenu;
	public GameObject CreditsMenu;
	public AudioMixer Mixer;
	public Slider MusicVolume;
	public Slider SoundVolume;

	private void Start()
	{
		MusicVolume.value = PlayerPrefs.GetFloat("MusicVolume", 0.0f);
		SoundVolume.value = PlayerPrefs.GetFloat("SoundVolume", 0.0f);
		MainMenu.GetComponentInChildren<Button>().Select();
	}

	public void NewGame()
	{
		GameProgress.Tutorial = true;
		GameProgress.Level = 1;
		SceneManager.LoadScene(GameProgress.GetLevelScene());
	}

	public void Continue()
	{
		GameProgress.Tutorial = false;
		SceneManager.LoadScene(GameProgress.GetLevelScene());
	}

	public void Options()
	{
		MainMenu.SetActive(false);
		OptionsMenu.SetActive(true);
		OptionsMenu.GetComponentInChildren<Button>().Select();
	}

	public void Credits()
	{
		MainMenu.SetActive(false);
		CreditsMenu.SetActive(true);
		CreditsMenu.GetComponentInChildren<Button>().Select();
	}

	public void Quit()
	{
		Application.Quit();
	}

	public void Back()
	{
		MainMenu.SetActive(true);
		MainMenu.GetComponentInChildren<Button>().Select();
		OptionsMenu.SetActive(false);
		CreditsMenu.SetActive(false);
	}

	public void SetMusic(float volume)
	{
		Mixer.SetFloat("MusicVolume", volume);
		PlayerPrefs.SetFloat("MusicVolume", volume);
	}

	public void SetSound(float volume)
	{
		Mixer.SetFloat("SoundVolume", volume);
		PlayerPrefs.SetFloat("SoundVolume", volume);
	}
}
