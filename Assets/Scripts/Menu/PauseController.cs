﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PauseController : MonoBehaviour
{
	public GameObject PauseMenu;
	public Text Title;
	public GameObject ContinueBtn;
	public GameObject RestartBtn;
	public GameObject QuitBtn;
	public AudioClip VictorySound;
	public AudioClip DefeatSound;

	private AudioSource _audio;
	private bool _paused = false;

	private void Awake()
	{
		_audio = GetComponent<AudioSource>();
	}

	// Update is called once per frame
	private void Update()
	{
		if (Input.GetButtonDown("Pause") && !GameProgress.Tutorial)
		{
			if (!PauseMenu.activeSelf)
			{
				Pause();
			}
		}
	}

	public void Pause()
	{
		_paused = true;
		Title.text = "PAUSED";
		ContinueBtn.SetActive(true);
		RestartBtn.SetActive(true);
		QuitBtn.SetActive(true);
		Time.timeScale = 0f;
		PauseMenu.SetActive(true);
		ContinueBtn.GetComponent<Button>().Select();
	}

	public void Victory()
	{
		Title.text = "VICTORY";
		ContinueBtn.SetActive(true);
		RestartBtn.SetActive(false);
		QuitBtn.SetActive(true);
		Time.timeScale = 0f;
		PauseMenu.SetActive(true);
		ContinueBtn.GetComponent<Button>().Select();
		_audio.clip = DefeatSound;
		_audio.Play();
		GameProgress.Level += 1;
	}

	public void Defeat()
	{
		Title.text = "DEFEAT";
		ContinueBtn.SetActive(false);
		RestartBtn.SetActive(true);
		QuitBtn.SetActive(true);
		Time.timeScale = 0f;
		PauseMenu.SetActive(true);
		RestartBtn.GetComponent<Button>().Select();
		_audio.clip = VictorySound;
		_audio.Play();
	}

	public void Continue()
	{
		Time.timeScale = 1f;
		if (_paused)
		{
			_paused = false;
			PauseMenu.SetActive(false);
		}
		else
		{
			SceneManager.LoadScene(GameProgress.GetLevelScene());
		}
	}

	public void Restart()
	{
		Time.timeScale = 1f;
		SceneManager.LoadScene(GameProgress.GetLevelScene());
	}

	public void Quit()
	{
		Time.timeScale = 1f;
		SceneManager.LoadScene("Menu");
	}
}
