﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialController : MonoBehaviour
{
	public GameObject[] Parts;
	public GameObject[] HidePlz;
	private int _currentPart = 0;

	// Use this for initialization
	private void Start()
	{
		if (GameProgress.Tutorial)
		{
			Time.timeScale = 0f;
			_currentPart = 0;
			Parts[_currentPart].SetActive(true);
			foreach (var hide in HidePlz)
			{
				hide.SetActive(false);
			}
		}
	}

	// Update is called once per frame
	private void Update()
	{
		if (GameProgress.Tutorial)
		{
			if (Input.GetButtonDown("Pause") || Input.GetButtonDown("Cancel"))
			{
				EndTutorial();
			}
			if (Input.GetButtonDown("Submit") || Input.GetMouseButtonDown(0))
			{
				Parts[_currentPart].SetActive(false);
				_currentPart++;
				if (_currentPart == Parts.Length)
				{
					EndTutorial();
				}
				else
				{
					Parts[_currentPart].SetActive(true);
				}
			}
		}
	}

	private void EndTutorial()
	{
		if (_currentPart < Parts.Length)
		{
			Parts[_currentPart].SetActive(false);
		}
		foreach (var hide in HidePlz)
		{
			hide.SetActive(true);
		}
		GameProgress.Tutorial = false;
		Time.timeScale = 1f;
	}
}
